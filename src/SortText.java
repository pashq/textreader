import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.lang.reflect.Array;
import java.util.*;

public class SortText {

    public static void main(String[] args) throws Exception {
        BufferedReader r = new BufferedReader(new FileReader("src/numbersText.txt"));
        String line;
        while ((line=r.readLine())!=null)
        {
            parseLine(line);
        }
    }
    private static void parseLine(String line)
    {
            int[] values = Arrays.stream(line.split(",")).mapToInt(Integer::parseInt).toArray();
            Arrays.sort(values);

            for(int i = 0; i <  values.length; i++) {
                System.out.print(values[i] + ", ");
            }

            System.out.print("\n");

            Integer[] reverseArray = Arrays.stream( values ).boxed().toArray( Integer[]::new );
            Arrays.sort(reverseArray, Collections.reverseOrder());

            for(int i = 0; i <  values.length; i++) {
                System.out.print(reverseArray[i] + ", ");
            }

    }
}
